package controller;

import java.util.ArrayList;
import java.util.List;

import lunchOrder.domain.LunchItem;

public class LunchInventory {

    private List<LunchItem> lunchItems;

    public LunchInventory() {
        this.lunchItems = loadLunchItems();
    }

    private List<LunchItem> loadLunchItems() {
        List<LunchItem> lunchItems = new ArrayList<>();

        lunchItems.add(new LunchItem(1, "Bruin Pistolet", 1.20, 10));
        lunchItems.add(new LunchItem(2, "Wit Pistolet", 1.20, 10));
        lunchItems.add(new LunchItem(3, "Bruine Boterham", 0.70, 10));
        lunchItems.add(new LunchItem(4, "Witte Boterham", 0.70, 10));

        lunchItems.add(new LunchItem(5, "Plak Kaas", 0.40, 15));
        lunchItems.add(new LunchItem(6, "Plak Ham", 0.40, 15));
        lunchItems.add(new LunchItem(7, "Hagelslag", 0.50, 15));
        lunchItems.add(new LunchItem(8, "Pindakaas", 0.50, 15));
        lunchItems.add(new LunchItem(9, "Jam", 0.30, 15));

        lunchItems.add(new LunchItem(10, "Melk", 1.00, 15));
        lunchItems.add(new LunchItem(11, "Karnemelk", 1.00, 15));
        lunchItems.add(new LunchItem(12, "Cola", 1.00, 15));

        lunchItems.add(new LunchItem(13, "Yoghurt", 1.30, 15));

        return lunchItems;
    }

    public LunchItem getLunchItem(int i) {
        for (LunchItem lunch : lunchItems) {
            if (lunch.getId() == i) {
                return lunch;
            }
        }
        throw new IllegalArgumentException("Niet bestaand lunchItem");
    }

    public void display() {
        for (LunchItem lunch : lunchItems) {
            System.out.println(lunch);
        }
    }
}
