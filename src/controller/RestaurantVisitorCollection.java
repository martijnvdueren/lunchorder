package controller;

import java.util.ArrayList;
import java.util.List;

import lunchOrder.domain.visitor.Employee;
import lunchOrder.domain.visitor.Guest;
import lunchOrder.domain.visitor.RestaurantVisitor;

public class RestaurantVisitorCollection {

    private List<RestaurantVisitor> visitors;

    public RestaurantVisitorCollection() {
        this.visitors = loadVisitors();
    }

    // Kunnen we via dit nog inserten?
    // Superclass of interface user, admin en employee eronder

    public RestaurantVisitor getVisitor(String username, String password) {
        for (RestaurantVisitor e : visitors) {
            if (e.getUsername().equals(username) && e.getPassword().equals(password)) {
                return e;
            }
        }
        return null;
    }

    private List<RestaurantVisitor> loadVisitors() {

        List<RestaurantVisitor> employees = new ArrayList<>();

        employees.add(new Employee("Edwin", "wachtwoord", 8));
        employees.add(new Guest("Frits", "wachtwoord"));
        employees.add(new Employee("Martijn", "wachtwoord", 8));
        employees.add(new Employee("Gerbert", "wachtwoord", 8));
        employees.add(new Employee("Nick", "wachtwoord", 8));
        employees.add(new Employee("Sander", "wachtwoord", 8));
        employees.add(new Employee("a", "a", 12));
        employees.add(new Guest("b", "b"));

        return employees;
    }

}
