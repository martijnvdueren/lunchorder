import java.util.Scanner;

import controller.LunchInventory;
import controller.RestaurantVisitorCollection;
import lunchOrder.domain.LunchItem;
import lunchOrder.domain.LunchOrder;
import lunchOrder.domain.menu.Menu;
import lunchOrder.domain.menu.MenuMaker;
import lunchOrder.domain.menu.MenuOptions;
import lunchOrder.domain.visitor.RestaurantVisitor;

public class Program {

    private static RestaurantVisitorCollection company = new RestaurantVisitorCollection();
    private static LunchInventory lunchInventory = new LunchInventory();
    private static RestaurantVisitor rVisitor;
    private static LunchOrder order;
    private static Menu menu;
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        myScanner();
    }

    public static void myScanner() {

        while (true) {

            System.out.println("Wat is je gebruikersnaam");
            String username = scan.nextLine();
            System.out.println("Wat is je wachtwoord?");
            String password = scan.nextLine();

            rVisitor = company.getVisitor(username, password);

            if (loggedIn()) {
                System.out.println(welcomeMessage(rVisitor.isPayingCustomer()));
                menu = MenuMaker.makeMenu(rVisitor.isPayingCustomer());
            }

            while (loggedIn()) {

                menu.printMenu();

                int choice = scan.nextInt(); // Waits for input
                scan.nextLine();

                switch (MenuOptions.program(choice)) {
                case LOG_OUT:
                    rVisitor = null;
                    System.out.println("Je bent uitgelogd.");
                    break;
                case SHOW_MENU:
                    System.out.println(MenuOptions.SHOW_MENU);
                    continue;
                case SHOW_BALANCE:
                    System.out.println(MenuOptions.SHOW_BALANCE);
                    showBalance();
                    continue;
                case ADD_BALANCE:
                    System.out.println(MenuOptions.ADD_BALANCE);
                    addBalance();
                    continue;
                case ORDER:
                    System.out.println(MenuOptions.ORDER);
                    makeOrder();
                    continue;
                case PRINT_ORDER:
                    System.out.println(MenuOptions.PRINT_ORDER);
                    printOrder();
                    continue;
                default:
                    System.out.println("error");
                    break;
                }
            }
        }
    }

    private static String welcomeMessage(boolean isPaying) {
        return "Welkom " + rVisitor.getUsername() + (isPaying ? " je hebt nog " + rVisitor.getBalance() + " euro op je rekening." : " lunch is gratis, eet smakelijk!");
    }

    private static boolean loggedIn() {
        return rVisitor != null;
    }

    private static void printOrder() {
        order.printReceipt();
    }

    private static void makeOrder() {
        System.out.println("\nMaak je keuze uit de volgende opties, kies 99 als je klaar bent");
        lunchInventory.display();

        order = new LunchOrder(rVisitor);

        while (true) {

            int choice = scan.nextInt();

            if (choice > 0 && choice < 14) {
                LunchItem lunchItem = lunchInventory.getLunchItem(choice);

                order.addLunchItem(lunchItem);
                System.out.println(lunchItem.getName() + " toegevoegd, kies 99 als je klaar bent");

            } else if (choice == 99) {
                System.out.println("Bedankt voor je bestelling");
                order.printReceipt();
                break;
            }

        }

    }

    private static void showBalance() {

        System.out.println("Je hebt " + String.format("%.2f", rVisitor.getBalance()) + " euro.");

    }

    private static void addBalance() {

        System.out.println("Je hebt " + String.format("%.2f", rVisitor.getBalance()) + " euro. Hoeveel geld wil je toevoegen?");

        rVisitor.addBalance(scan.nextInt());

    }

}
