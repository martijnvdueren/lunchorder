package lunchOrder.domain.visitor;

public class Guest implements RestaurantVisitor {

    private String username;
    private String password;

    public Guest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean payLunch(double amount) {
        System.out.println("Je bent een gast van het bedrijf, lunch is gratis!");
        return true;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    // We give our guests free lunch!
    public boolean isPayingCustomer() {
        return false;
    }

    @Override
    // Guests don't have balance
    public double getBalance() {
        return 0;
    }

    @Override
    // Guests don't have balance
    public void addBalance(double amount) {
    }

}
