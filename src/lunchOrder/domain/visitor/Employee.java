package lunchOrder.domain.visitor;

import lunchOrder.domain.Wallet;

public class Employee implements RestaurantVisitor {

    private String username;
    private String password;
    private Wallet wallet;

    public Employee(String username, String password, double initialBalance) {
        this.username = username;
        this.password = password;
        this.wallet = new Wallet(initialBalance);
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean payLunch(double amount) {
        if (this.getBalance() >= amount) {
            this.wallet.pay(amount);
            return true;
        } else {
            System.out.println("Niet genoeg salde (" + this.getBalance() + ") om je lunch te betalen! (" + amount + ")");
            return false;
        }
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public double getBalance() {
        return this.wallet.getBalance();
    }

    @Override
    public void addBalance(double amount) {
        this.wallet.addBalance(amount);
        System.out.println(amount + " toegevoegd. Huidig saldo: " + this.getBalance());
    }

    @Override
    // Sorry employees have to pay, bring your own if you don't like it!
    public boolean isPayingCustomer() {
        return true;
    }

}
