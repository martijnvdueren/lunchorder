package lunchOrder.domain.visitor;

public interface RestaurantVisitor {

    public String getUsername();

    public String getPassword();

    public boolean isPayingCustomer();

    public double getBalance();

    public void addBalance(double amount);

    public boolean payLunch(double amount);

}
