package lunchOrder.domain.menu;

public final class MenuMaker {

    public static Menu makeMenu(boolean isPayingCustomer) {

        if (isPayingCustomer) {
            return new EmployeeMenu();
        }

        return new GuestMenu();

    }
}
