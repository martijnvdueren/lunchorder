package lunchOrder.domain.menu;
import java.util.Arrays;

public class GuestMenu implements Menu {

    @Override
    public void printMenu() {
        Arrays.stream(MenuOptions.values()).filter(MenuOptions::isShownToGuest).forEach(System.out::println);
    }

}
