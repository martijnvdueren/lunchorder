package lunchOrder.domain.menu;

import java.util.Arrays;

public class EmployeeMenu implements Menu {

    @Override
    public void printMenu() {
        Arrays.stream(MenuOptions.values()).forEach(System.out::println);
    }

}
