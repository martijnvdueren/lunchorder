package lunchOrder.domain.menu;

public enum MenuOptions {

    SHOW_MENU(1, true, "Toon hoofdmenu"),
    ADD_BALANCE(2, false, "Voeg geld toe aan je rekening"),
    SHOW_BALANCE(3, false, "Toon huidig saldo"),
    ORDER(4, true, "Maak je lunchbestelling"),
    LOG_OUT(5, true, "Log uit"),
    PRINT_ORDER(6, true, "Toon order");

    private int number;
    private boolean isShownToGuest;
    private String description;

    MenuOptions(int number, boolean isShownToGuest, String description) {
        this.number = number;
        this.isShownToGuest = isShownToGuest;
        this.description = description;
    }

    public static MenuOptions program(int i) {
        for (MenuOptions p : MenuOptions.values()) {
            if (p.number == i) {
                return p;
            }
        }
        return null;
    }

    public int getNumber() {
        return number;
    }

    public String getDescription() {
        return description;
    }

    public boolean isShownToGuest() {
        return isShownToGuest;
    }

    @Override
    public String toString() {
        return number + ") " + description;
    }

}
