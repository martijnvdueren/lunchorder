package lunchOrder.domain;

import java.util.ArrayList;
import java.util.List;

import lunchOrder.domain.visitor.RestaurantVisitor;

public class LunchOrder {
    private List<LunchItem> order;
    private RestaurantVisitor rVisitor;

    public LunchOrder(RestaurantVisitor rVisitor) {
        super();
        this.order = new ArrayList<>();
        this.rVisitor = rVisitor;
    }

    public void addLunchItem(LunchItem item) {
        this.order.add(item);
    }

    public double getLunchTotal() {
        return order.stream().map(i -> i.getPrice()).reduce(0d, Double::sum);
    }

    public void checkOut() {
        rVisitor.payLunch(getLunchTotal());
    }

    public void printReceipt() {
        System.out.println("==== Lunch Bestelling ====\n");

        for (LunchItem item : order) {
            System.out.println(item);
        }

        if (rVisitor.isPayingCustomer()) {
            System.out.println("\n====Totaal====\n� " + String.format("%.2f", getLunchTotal()) + "\n");
        } else {
            System.out.println("Je lunch is gratis, eet smakelijk.");
        }

    }

}
