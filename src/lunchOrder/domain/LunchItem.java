package lunchOrder.domain;

public class LunchItem {

    private int id;
    private String name;
    private double price;
    private double availableAmount;

    public LunchItem(int id, String name, double price, double availableAmount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.availableAmount = availableAmount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getAvailableAmount() {
        return availableAmount;
    }

    @Override
    public String toString() {
        return id + ") " + name + " �" + String.format("%.2f", price);
    }

}
